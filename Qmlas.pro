QT       += core gui charts network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    darkstyle.cpp \
    detaconnection.cpp \
    dialognewproject.cpp \
    dialogopenproject.cpp \
    dragdropfilesystemmodel.cpp \
    gefcpt.cpp \
    main.cpp \
    mainwindow.cpp \
    mlasprojectwidget.cpp \
    project.cpp

HEADERS += \
    darkstyle.h \
    detaconnection.h \
    dialognewproject.h \
    dialogopenproject.h \
    dragdropfilesystemmodel.h \
    gefcpt.h \
    mainwindow.h \
    mlasprojectwidget.h \
    project.h \
    secrets.h

FORMS += \
    dialognewproject.ui \
    dialogopenproject.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
