#include "detaconnection.h"
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

DetaConnection::DetaConnection(QObject *parent) : QObject(parent)
{

}

void DetaConnection::getLeveeCodes(const QString owner)
{
    QStringList result;

    const QString url_string = QString("%1/leveecodes?owner=%2").arg(DETA_URL, owner);
    qDebug() << url_string;

    QNetworkRequest req = QNetworkRequest(QUrl(url_string));
    m_networkaccesmanager.get(req);
    connect(&m_networkaccesmanager, &QNetworkAccessManager::finished, this, &DetaConnection::onLeveeCodesResult);
}

void DetaConnection::onLeveeCodesResult(QNetworkReply *reply)
{
    QByteArray response_data = reply->readAll();
    QJsonDocument json_response = QJsonDocument::fromJson(response_data);
    QJsonObject json_object = json_response.object();
    QJsonArray json_array = json_object["leveecodes"].toArray();
    foreach(const QJsonValue &value, json_array){
        QJsonObject obj = value.toObject();
        qDebug() << obj["code"].toString();
    }
    reply->deleteLater();
    disconnect(&m_networkaccesmanager, 0,0,0);
}
