#ifndef DETACONNECTION_H
#define DETACONNECTION_H

#include <QObject>
#include <QNetworkAccessManager>

#include <secrets.h>

class DetaConnection : public QObject
{
    Q_OBJECT
public:
    explicit DetaConnection(QObject *parent = nullptr);

    void getLeveeCodes(const QString owner);

signals:
    void receivedLeveeCodes(QStringList leveecode);

private slots:
    void onLeveeCodesResult(QNetworkReply *reply);



private:
    QNetworkAccessManager m_networkaccesmanager;

};

#endif // DETACONNECTION_H
