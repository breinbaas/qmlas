#include "dialognewproject.h"
#include "ui_dialognewproject.h"

#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QPushButton>

DialogNewProject::DialogNewProject(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogNewProject)
{
    ui->setupUi(this);

    QRegularExpression rx("^[A-Za-z0-9_-]*$");
    QValidator *validator = new QRegularExpressionValidator(rx, this);
    ui->leProjectName->setValidator(validator);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

DialogNewProject::~DialogNewProject()
{    
    delete ui;
}

void DialogNewProject::on_leProjectName_textChanged(const QString &arg)
{
    if(arg.length()==0){
        ui->lblWarning->setText("Warning: invalid name");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }else if (m_taken_projectnames.contains(arg)){
        ui->lblWarning->setText("Warning: project name already used");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }else{
        ui->lblWarning->setText("");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
    m_project_name = arg;
}

