#ifndef DIALOGNEWPROJECT_H
#define DIALOGNEWPROJECT_H

#include <QDialog>

namespace Ui {
class DialogNewProject;
}

class DialogNewProject : public QDialog
{
    Q_OBJECT

public:
    explicit DialogNewProject(QWidget *parent = nullptr);
    ~DialogNewProject();

    QString projectName() const { return m_project_name; }

    void addTakenProjectName(const QString project_name){
        m_taken_projectnames.append(project_name);
    }

private slots:
    void on_leProjectName_textChanged(const QString &arg1);

private:
    Ui::DialogNewProject *ui;
    QString m_project_name;
    QStringList m_taken_projectnames;
};

#endif // DIALOGNEWPROJECT_H
