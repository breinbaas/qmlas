#include "dialogopenproject.h"
#include "ui_dialogopenproject.h"

DialogOpenProject::DialogOpenProject(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogOpenProject)
{
    ui->setupUi(this);
    m_project_name = "";
}

DialogOpenProject::~DialogOpenProject()
{
    delete ui;
}

void DialogOpenProject::setRootDir(const QDir root)
{
    for (const QFileInfo &finfo: root.entryInfoList(QDir::NoDotAndDotDot | QDir::AllDirs)) {
      if(finfo.isDir())
          ui->lwProjects->addItem(finfo.fileName());
    }

    if(ui->lwProjects->count()>0){
        ui->lwProjects->setCurrentRow(0);
    }
}

void DialogOpenProject::on_lwProjects_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    Q_UNUSED(current);
    Q_UNUSED(previous);
    m_project_name = ui->lwProjects->currentItem()->text();
}

