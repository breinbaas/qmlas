#ifndef DIALOGOPENPROJECT_H
#define DIALOGOPENPROJECT_H

#include <QDialog>
#include <QDir>
#include <QListWidgetItem>

namespace Ui {
class DialogOpenProject;
}

class DialogOpenProject : public QDialog
{
    Q_OBJECT



public:
    explicit DialogOpenProject(QWidget *parent = nullptr);
    ~DialogOpenProject();

    void setRootDir(const QDir root);
    QString getProjectName() const { return m_project_name; }

private slots:
    void on_lwProjects_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

private:
    Ui::DialogOpenProject *ui;

    QDir rootDir;
    QString m_project_name;
};

#endif // DIALOGOPENPROJECT_H
