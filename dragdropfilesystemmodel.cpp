#include "dragdropfilesystemmodel.h"

DragDropFileSystemModel::DragDropFileSystemModel(QObject *parent) : QFileSystemModel(parent)
{

}

Qt::ItemFlags DragDropFileSystemModel::flags(const QModelIndex &index)
{
    Qt::ItemFlags defaultFlags = QFileSystemModel::flags(index);

    if (!index.isValid())
        return defaultFlags;

    const QFileInfo& fileInfo = this->fileInfo(index);

//    // The target
//    if (fileInfo.isFile())
//    {
//        // allowed drop
//        return Qt::ItemIsDropEnabled | defaultFlags;
//    }
    // The source: should be directory (in that case)
    if (fileInfo.isFile())
    {
        // allowed drop
        return Qt::ItemIsDropEnabled | defaultFlags;
    }

    return defaultFlags;
}
