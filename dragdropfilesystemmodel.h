#ifndef DRAGDROPFILESYSTEMMODEL_H
#define DRAGDROPFILESYSTEMMODEL_H

#include <QObject>
#include <QFileSystemModel>

class DragDropFileSystemModel : public QFileSystemModel
{
    Q_OBJECT
public:
    explicit DragDropFileSystemModel(QObject *parent = nullptr);


signals:

protected:
    Qt::ItemFlags flags(const QModelIndex &index);

};

#endif // DRAGDROPFILESYSTEMMODEL_H
