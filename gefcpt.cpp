#include "gefcpt.h"
#include <QFile>
#include <QTextStream>
#include <QLineSeries>
#include <QValueAxis>
#include <QFileInfo>

const double QCMAX = 50.0;
const double FNMAX = 10.0;

GEFCPT::GEFCPT(QObject *parent) : QObject(parent)
{

}

bool GEFCPT::read_from_file(const QString fileName)
{
    QFile inputFile(fileName);
    mErrorReport.clear();

    //setup some variables
    int colQc = -1;
    int colFs = -1;
    int colZ = -1;
    QString colSeparator = " ";
    QMap<int, int> columnVoids;

    if (inputFile.open(QIODevice::ReadOnly))
    {
        mErrorReport.append(QString("Reading '%1'").arg(fileName));
        QTextStream in(&inputFile);

        // reading header
        bool readHeader = true;
        while (!in.atEnd() & readHeader)
        {
            QString line = in.readLine();
            QStringList arglines = line.split('=');

            if(arglines.length()<2){
                mErrorReport.append(QString("Fatal error, invalid header line '%1'").arg(line));
                return false;
            }
            QStringList args = arglines[1].split(",");

            if(line.contains("#ZID")){
                if(args.count()<2){
                    mErrorReport.append(QString("Fatal error reading #ZID in line '%1'").arg(line));
                    return false;
                }

                bool ok;
                mZStart = args[1].toDouble(&ok);
                if(!ok){
                    mErrorReport.append(QString("Fatal error reading #ZID in line '%1'").arg(line));
                    return false;
                }
            }else if(line.contains("#COLUMNINFO")){
                if(args.count()!=4){
                    mErrorReport.append(QString("Fatal error reading #COLUMNINFO in line '%1'").arg(line));
                    return false;
                }

                bool colIdOk;
                bool colIndexOk;
                int colId = args[3].toInt(&colIdOk);
                int colIndex = args[0].toInt(&colIndexOk);

                if(colIdOk & colIndexOk){
                    switch (colId) {
                        case 1: colZ == -1 ? colZ = colIndex : colZ; break;
                        case 2: colQc = colIndex; break;
                        case 3: colFs = colIndex; break;
                        case 11: colZ = colIndex; break;
                    }
                }else{
                    mErrorReport.append(QString("Fatal error reading #COLUMNINFO in line '%1'").arg(line));
                    return false;
                }
            }else if(line.contains("#EOH")){
                readHeader = false;
            }else if(line.contains("#COLUMNSEPARATOR")){
                if(args.count()<1){
                    mErrorReport.append(QString("Fatal error reading #COLUMNSEPARATOR in line '%1'").arg(line));
                    return false;
                }
                colSeparator = args[0].trimmed();
            }else if(line.contains("#COLUMNVOID")){
                if(args.count()!=2){
                    mErrorReport.append(QString("Fatal error reading #COLUMNVOID in line '%1'").arg(line));
                    return false;
                }

                bool colIndexOk;
                bool valueOk;
                int colIndex = args[0].toInt(&colIndexOk);
                double value = args[1].toDouble(&valueOk);

                if(colIndexOk & valueOk){
                    columnVoids[colIndex] = int(value);
                }else{
                    mErrorReport.append(QString("Fatal error reading #COLUMNVOID in line '%1'").arg(line));
                    return false;
                }
            }
        }

        if((colZ==-1) | (colQc==-1) | (colFs==-1)){
            mErrorReport.append("Fatal error; could not find #COLUMNINFO for one or more of qc, fs and z");
            return false;
        }

        // subtract one from the columns due to indexing from 0
        colZ -= 1;
        colQc -= 1;
        colFs -= 1;
        int colMax = qMax(qMax(colZ, colQc), colFs);

        // reading data
        while (!in.atEnd()){
            QString line = in.readLine();
            QStringList args = line.split(colSeparator);


            QStringList finalArgs;
            for (int i=0; i<args.count(); i++){
                if(args[i].replace(" ","").length()>0){
                    finalArgs.append(args[i]);
                }
            }

            if(finalArgs.length()<colMax+1){
                mErrorReport.append(QString("Error in data (expected at least %1 columns, got %2), skipping line '%3'").arg(colMax+1).arg(finalArgs.length()).arg(line));
                return 1;
            }else{
                double z = finalArgs[colZ].toDouble();
                double qc = finalArgs[colQc].toDouble();
                double fs = finalArgs[colFs].toDouble();

                // check for columnvoids
                bool validReading = true;
                QMap<int, int>::iterator i;
                for(i=columnVoids.begin(); i!=columnVoids.end(); i++){
                    if(i.key()==colZ)
                        validReading &= z!=i.value();
                    else if (i.key()==colFs)
                        validReading &= fs != i.value();
                    else if (i.key()==colQc)
                        validReading &= qc != i.value();
                }

                if(validReading){
                    GEFReading reading;
                    reading.z = mZStart - qAbs(z); // sometimes z>0 sometimes z<0, now we are sure..
                    reading.qc = qc;
                    reading.fs = fs;

                    if(qc<=0) qc = 1e-3;
                    reading.fn = fs * 100. / qc;
                    mReadings.append(reading);
                }
            }
        }

        QFileInfo fileInfo(inputFile.fileName());
        mFilename = fileInfo.fileName();
        mFullFilename = fileName;
        inputFile.close();
    }else{
        mErrorReport.append(QString("Missing file or error(s) in file '%1'").arg(fileName));
        return false;
    }

    return true;
}

QChart *GEFCPT::get_chart(QColor backgroundColor)
{
    //for 0.1 we will use an axis on y with a 1m interval
    double zmax;
    if(mZStart>=0){
        zmax = int(mZStart) + 1.0;
    }else{
        zmax = int(mZStart);
    }

    double zmin = mReadings[mReadings.count()-1].z;
    if(zmin>=0){
        zmin = int(zmin) + 1;
    }else{
        zmin = int(zmin) - 1;
    }

    QChart *chart = new QChart();
    chart->setBackgroundBrush(QBrush(backgroundColor));
    //chart->legend()->setColor(Qt::white);
    chart->legend()->setLabelColor(Qt::white);


    QValueAxis *axisQc = new QValueAxis;
    QValueAxis *axisZ = new QValueAxis;
    QValueAxis *axisFs = new QValueAxis;
    QValueAxis *axisFn = new QValueAxis;

    axisQc->setRange(0, QCMAX);
    axisQc->setTickCount(11);
    axisFs->setRange(0, 1.0);
    axisFs->setTickCount(11);
    axisFn->setRange(0, 50);
    axisFn->setTickCount(11);
    axisFn->setReverse(true);
    axisZ->setRange(zmin, zmax);
    axisZ->setTickCount(zmax - zmin + 1);

    chart->addAxis(axisQc, Qt::AlignTop);
    chart->addAxis(axisFs, Qt::AlignBottom);
    chart->addAxis(axisZ, Qt::AlignLeft);
    chart->addAxis(axisFn, Qt::AlignBottom);

    axisQc->setLabelsBrush(QBrush(QColor(Qt::white)));
    axisFs->setLabelsBrush(QBrush(QColor(Qt::white)));
    axisZ->setLabelsBrush(QBrush(QColor(Qt::white)));
    axisFn->setLabelsBrush(QBrush(QColor(Qt::white)));

    QLineSeries *seriesQc = new QLineSeries();
    QLineSeries *seriesFs = new QLineSeries();
    QLineSeries *seriesFn = new QLineSeries();

    for(int i=0; i<mReadings.count(); i++){
        if(mReadings[i].qc>QCMAX){
            seriesQc->append(QCMAX, mReadings[i].z);
        }else{
            seriesQc->append(mReadings[i].qc, mReadings[i].z);
        }
        seriesFs->append(mReadings[i].fs, mReadings[i].z);
        if(mReadings[i].fn>FNMAX){
            seriesFn->append(FNMAX, mReadings[i].z);
        }else{
            seriesFn->append(mReadings[i].fn, mReadings[i].z);
        }
    }

    chart->addSeries(seriesQc);
    chart->addSeries(seriesFs);
    chart->addSeries(seriesFn);

    seriesQc->attachAxis(axisQc);
    seriesQc->attachAxis(axisZ);
    seriesFs->attachAxis(axisFs);
    seriesFs->attachAxis(axisZ);
    seriesFn->attachAxis(axisFn);
    seriesFn->attachAxis(axisZ);

    seriesQc->setName("qc [MPa]");
    seriesFs->setName("fs [MPa]");
    seriesFn->setName("Rf [%]");

    chart->setTitle(mFilename);

    return chart;
}

