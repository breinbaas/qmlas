#ifndef GEFCPT_H
#define GEFCPT_H

#include <QObject>
#include <QtCharts/QChartView>

using namespace QtCharts;

struct GEFReading{
    double z;
    double qc;
    double fs;
    double fn;
};

class GEFCPT : public QObject
{
    Q_OBJECT
public:
    explicit GEFCPT(QObject *parent = nullptr);

    bool read_from_file(const QString filename);
    QChart *get_chart(QColor backgroundColor);
    QString filename() {return mFilename;}
    QString full_filename() {return mFullFilename;}
    QStringList* error_report() {return &mErrorReport;}

private:
    QString mFilename;
    QString mFullFilename;
    QList<GEFReading> mReadings;
    QStringList mErrorReport;
    double mZStart;

signals:

};

#endif // GEFCPT_H
