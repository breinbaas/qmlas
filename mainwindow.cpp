#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QFileDialog>

#include <dialognewproject.h>
#include <dialogopenproject.h>
#include <project.h>
#include <gefcpt.h>


#include <QMessageBox>

#include <QDebug>

const QString DEFAULTPATH = "D:/Data/Documents/qmlas";
const QString OWNER = "waternet";


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_project = nullptr;

    //m_filesystem_model = new DragDropFileSystemModel(this);
    //m_filesystem_model->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::AllEntries);
    //m_filesystem_model->setRootPath(DEFAULTPATH);

    m_filesystem_model.setFilter(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::AllEntries);
    m_filesystem_model.setRootPath(DEFAULTPATH);

    m_deta_connection.getLeveeCodes(OWNER);


    setWindowTitle("qMLAS");

    // preview frame setup
    m_frame_right_layout = new QGridLayout(ui->frameRight);

    //widget for the cpts and boreholes on the crest
    QGridLayout *layout1 = new QGridLayout(ui->frameCPTBoreholeCrest);
    m_project_widget = new MLASProjectWidget(ui->frameCPTBoreholeCrest);
    layout1->addWidget(m_project_widget);

    ui->tvProject->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tvProject->setDragEnabled(true);
    ui->tvProject->viewport()->setAcceptDrops(true);
    ui->tvProject->setDropIndicatorShown(true);
    ui->tvProject->setDragDropMode(QAbstractItemView::InternalMove);
    ui->tvProject->setAcceptDrops(true);
    //ui->tvProject->setModel(&m_filesystem_model);
}

MainWindow::~MainWindow()
{
    if(m_project != nullptr){
        delete m_project;
    }

    delete ui;
}


void MainWindow::on_actionNewProject_triggered()
{
    DialogNewProject *dlg = new DialogNewProject(this);

    QDir dir = QDir(DEFAULTPATH);

    // check the root path to add existing project names
    // and prevent people to create a project with the same name
    for (const QFileInfo &finfo: dir.entryInfoList()) {
      if(finfo.isDir())
        dlg->addTakenProjectName(finfo.fileName());
    }

    if(dlg->exec()){
        QString projectName = dlg->projectName();

        if(m_project != nullptr){
            delete m_project;
            m_project = nullptr;
            ui->tvProject->setModel(nullptr);
        }
        m_project = new Project(QDir(DEFAULTPATH), projectName);
        if (m_project->createPaths()){
            m_filesystem_model.setRootPath(m_project->rootDir().path());
            ui->tvProject->setModel(&m_filesystem_model);
            ui->tvProject->hideColumn(1);
            ui->tvProject->hideColumn(2);
            ui->tvProject->hideColumn(3);
            ui->tvProject->setRootIndex(m_filesystem_model.index(m_project->rootDir().path()));
            setWindowTitle(QString("%1 : %2").arg("qMLAS", m_project->name()));
        }else{
            QMessageBox::warning(this, "qMLAS", "Error creating the default project structure, please check your permissions for the chosen directory", QMessageBox::Ok);
        }
    }
}


void MainWindow::on_actionOpenProject_triggered()
{
    DialogOpenProject *dlg = new DialogOpenProject(this);
    dlg->setRootDir(QDir(DEFAULTPATH));

    if(dlg->exec()){
        if(!dlg->getProjectName().isEmpty()){
            QDir dir = QDir(DEFAULTPATH + '/' + dlg->getProjectName());

            if(m_project != nullptr){
                delete m_project;
                m_project = nullptr;
                ui->tvProject->setModel(nullptr);
            }

            m_project = new Project(DEFAULTPATH, dlg->getProjectName());
            if(m_project->checkPaths()){
                m_filesystem_model.setRootPath(dir.path());
                ui->tvProject->setModel(&m_filesystem_model);
                ui->tvProject->hideColumn(1);
                ui->tvProject->hideColumn(2);
                ui->tvProject->hideColumn(3);
                ui->tvProject->setRootIndex(m_filesystem_model.index(dir.path()));
                setWindowTitle(QString("%1 : %2").arg("qMLAS", m_project->name()));
            }else{
                QMessageBox::warning(this, "qMLAS", "This seems to be an invalid project, please check the directory", QMessageBox::Ok);
                delete m_project;
                m_project = nullptr;
            }
        }
    }
}


void MainWindow::on_tvProject_clicked(const QModelIndex &index)
{
    if(m_project==nullptr) return;

    QVariant data = m_filesystem_model.filePath(index);
    QString filename = data.toString();

    QFileInfo fi(filename);
    if(fi.exists()){
        previewFile(fi);
    }
}

void MainWindow::previewFile(const QFileInfo fi)
{
    if(fi.suffix().toLower()=="gef"){
        previewGEFFile(fi);
    }
}

void MainWindow::previewGEFFile(const QFileInfo fi)
{
    if(!fi.exists()) return;

    //remove all widgets from the right frame
    qDeleteAll(ui->frameRight->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly));

    GEFCPT *gefcpt = new GEFCPT();
    if(gefcpt->read_from_file(fi.absoluteFilePath())){
        //add the chartview
        QChartView *cpt_chartview = new QChartView(ui->frameRight);
        cpt_chartview->setRubberBand(QChartView::VerticalRubberBand);
        cpt_chartview->setBackgroundBrush(QBrush(QColor(53, 53, 53)));

        m_frame_right_layout->addWidget(cpt_chartview);
        QChart* newChart = gefcpt->get_chart(QColor(53, 53, 53));
        cpt_chartview->setChart(newChart);
        cpt_chartview->show();
    }else{
        //add the no preview
        QLabel *lblPreview = new QLabel(ui->frameRight);
        //lblPreview->setStyleSheet("QLabel { background-color : #535353; color : white; }");
        lblPreview->setText("could not preview GEFCPT");
        m_frame_right_layout->addWidget(lblPreview);
    }

}

