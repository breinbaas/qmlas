#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QChartView>
#include <QLabel>
#include <QGridLayout>

#include <project.h>
#include <mlasprojectwidget.h>
#include <dragdropfilesystemmodel.h>
#include <detaconnection.h>

using namespace QtCharts;


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionNewProject_triggered();
    void on_actionOpenProject_triggered();
    void on_tvProject_clicked(const QModelIndex &index);

private:
    Ui::MainWindow *ui;
    Project *m_project;
    MLASProjectWidget *m_project_widget;

    QGridLayout *m_frame_right_layout;
    DetaConnection m_deta_connection;

    //DragDropFileSystemModel *m_filesystem_model;

    QFileSystemModel m_filesystem_model;

    //QMapControl* mMapControl;

//    QChartView *m_cpt_chartview;

//    QLayout *m_layout_frame_right;
//    QLabel *m_lbl_preview;


    QFileSystemModel *dirModel;
    void createNewProject(const QString projectname);
    void previewFile(const QFileInfo fi);
    void previewGEFFile(const QFileInfo fi);


};
#endif // MAINWINDOW_H
