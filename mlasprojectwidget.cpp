#include "mlasprojectwidget.h"

#include <QPainter>
#include <QDebug>

MLASProjectWidget::MLASProjectWidget(QWidget *parent) : QWidget( parent)
{

}

void MLASProjectWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QPen whitePen = QPen(Qt::white);
    painter.drawLine(0,0,width(),height());

    painter.setPen(QPen(Qt::black));

}
