#ifndef MLASPROJECTWIDGET_H
#define MLASPROJECTWIDGET_H

#include <QWidget>

class MLASProjectWidget : public QWidget
{
public:
    MLASProjectWidget(QWidget *parent);

protected:
  void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

};

#endif // MLASPROJECTWIDGET_H
