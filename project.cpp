#include "project.h"
#include <QString>
#include <QDir>

Project::Project(const QDir rootDir, const QString projectName)
{    
    m_root_dir = QDir(rootDir.path() + "/" + projectName);
    m_name = projectName;
    m_mandatory_dirs = QStringList() << "grondonderzoek/sonderingen/kruin" << "grondonderzoek/sonderingen/teen" << "grondonderzoek/boringen/kruin" << "grondonderzoek/boringen/teen" << "dwarsprofielen" ;
}


bool Project::createPaths()
{
    bool succes = true;
    foreach(QString dirName, m_mandatory_dirs){
        dirName = m_root_dir.absolutePath() + '/' + dirName;
        QDir newDir = QDir(dirName);
        if(!newDir.exists()){
            succes &= QDir().mkpath(dirName);
        }
    }
    return succes;
}

bool Project::checkPaths()
{
    bool succes = true;
    foreach(QString dirName, m_mandatory_dirs){
        dirName = m_root_dir.absolutePath() + '/' + dirName;
        succes &= QDir(dirName).exists();
    }
    return succes;
}
