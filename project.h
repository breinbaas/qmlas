#ifndef PROJECT_H
#define PROJECT_H

#include <QObject>
#include <QDir>
#include <QString>


class Project
{
public:
    Project(const QDir rootDir, const QString projectName);

    //bool fromDir(const QDir dir);

    bool createPaths();
    bool checkPaths();
    QString name() const { return m_name; }
    QDir rootDir() const { return m_root_dir; }

    void setName(const QString name) {m_name = name;}

private:
    QDir m_root_dir;
    QStringList m_mandatory_dirs;
    QString m_name;
};

#endif // PROJECT_H
